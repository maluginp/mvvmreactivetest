﻿using System;
using ReactiveUI;
using MvvmReactiveTest.Core.Models;
using MvvmReactiveTest.Core.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MvvmReactiveTest.ViewModels
{
	public class ProjectsViewModel : ReactiveObject, IProjectsViewModel {
		public ReactiveList<IProjectViewModel> Projects {
			get;
			protected set;
		}

		public ReactiveCommand<IObservable<IProjectViewModel>> RefreshCommand {
			get;
			protected set;
		}

		public ReactiveCommand<IEnumerable<IProject>> LoadCommand {
			get;
			protected set;
		}

		IDataService _dataService;
		ProjectsModel _model;

		public ProjectsViewModel (ProjectsModel projectsModel) {
			_model = projectsModel;

			Projects = new ReactiveList<IProjectViewModel> ();

			var canLoad = this.WhenAnyValue (vm => vm.Projects, x => x.Count == 0);
			LoadCommand = ReactiveCommand.CreateAsyncTask(canLoad, _ => {
				Console.WriteLine("LoadCommand::Task");
				return _model.List();			
			});


			LoadCommand.Subscribe (projects => {
				Console.WriteLine("LoadCommand::subscribe");
				Projects.Clear();
				foreach (IProject project in projects) {
					Projects.Add (new ProjectViewModel (project));
				}

			});
		

			Initialize ();

		}

		private async void Initialize() {
//			IEnumerable<IProject> projects = await _model.List ();
//
//			foreach (IProject project in projects) {
//				Projects.Add (new ProjectViewModel (project));
//			}

//			Projects = new ReactiveList<IProject> ();
//			RefreshCommand = ReactiveCommand.CreateAsyncTask(_ => {
//				return Task.Run(async () => {
//					IEnumerable<IProject> refreshedProjects = await _model.List();
//					Projects.AddRange(projects);	
//					return refreshedProjects;
//				});
//			});

		}


		
	}
}

