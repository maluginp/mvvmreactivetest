﻿using System;
using ReactiveUI;
using MvvmReactiveTest.Core.Models;

namespace MvvmReactiveTest
{
	public class ProjectViewModel : ReactiveObject, IProjectViewModel {

		public ProjectViewModel (IProject project) {
			Id = project.Id;
			Name = project.Name;
			Author = project.Author;
			Done = project.Done;
			UpdatedAt = project.UpdatedAt;
		}
		
		private int _Id;
		public int Id {
			get {
				return _Id;
			}
			set {
				this.RaiseAndSetIfChanged (ref _Id, value);
			}
		}

		private string _Name;
		public string Name {
			get {
				return _Name;
			}
			set {
				this.RaiseAndSetIfChanged (ref _Name, value);
			}
		}

		private string _Author;
		public string Author {
			get {
				return _Author;
			}
			set {
				this.RaiseAndSetIfChanged (ref _Author, value);
			}
		}

		private bool _Done;
		public bool Done {
			get {
				return _Done;
			}
			set {
				this.RaiseAndSetIfChanged (ref _Done, value);
			}
		}

		private DateTime _UpdatedAt;
		public DateTime UpdatedAt {
			get {
				return _UpdatedAt;
			}
			set {
				this.RaiseAndSetIfChanged (ref _UpdatedAt, value);
			}
		}

	}
}

