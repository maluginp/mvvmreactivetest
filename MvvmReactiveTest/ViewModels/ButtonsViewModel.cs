﻿using System;
using ReactiveUI;
using Rx = System.Reactive;

namespace MvvmReactiveTest
{
	public class ButtonsViewModel : ReactiveObject, IButtonsViewModel
	{
		public ReactiveCommand<object> UpdateCommand {
			get;
			protected set;
		}

		public ReactiveCommand<object> ClearCommand {
			get;
			protected set;
		}

		private string _Label;
		public string Label {
			get {
				return _Label;
			}
			set {
				Console.WriteLine ("Set Label: {0}", value);
				this.RaiseAndSetIfChanged (ref _Label, value);
			}
		}

		ObservableAsPropertyHelper<bool> _loadingModels;
		public bool LoadingModels {
			get { return _loadingModels.Value; }
		}

		public ButtonsViewModel () {

			var canUpdate = this.WhenAny (vm => vm.Label, s => string.IsNullOrEmpty (s.Value));
			UpdateCommand = ReactiveCommand.Create (canUpdate);
			UpdateCommand.Subscribe (_ => {
				Label = "Updated";
				Console.WriteLine("ButtonsViewModel::UpdateCommand::subscribe");
			});


			UpdateCommand.IsExecuting.ToProperty(this, x=> x.LoadingModels, out _loadingModels);

			var canClear = this.WhenAny (vm => vm.Label, s => !string.IsNullOrEmpty (s.Value));
			ClearCommand = ReactiveCommand.Create (canClear);
			ClearCommand.Subscribe (x => {
				Label = "";
				Console.WriteLine("ButtonsViewModel::ClearCommand::subscribe");
			});

		}
	}
}

