﻿using System;

namespace MvvmReactiveTest.Core.Models
{
	public interface IProject
	{
		int Id {get;set;}
		string Name { get; set; }
		string Author { get; set; }
		bool Done {get;set;}
		DateTime UpdatedAt { get; set; }
	}
}

