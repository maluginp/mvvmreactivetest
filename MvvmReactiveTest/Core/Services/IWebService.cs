﻿using System;
using MvvmReactiveTest.Core;
using MvvmReactiveTest.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MvvmReactiveTest
{
	public interface IWebService {
		Task<IEnumerable<IProject>> GetProjects(int page=0, int count = Constants.ProjectsInPage);

	}
}

