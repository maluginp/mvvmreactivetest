﻿using System;
using System.Linq.Expressions;
using MvvmReactiveTest.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace MvvmReactiveTest
{
	public interface IDataService {
		Task<IEnumerable<IProject>> GetProjects(Expression<Func<IProject,bool>> filter);
		Task<IProject> GetProject (int projectId);
		Task<bool> UpdateProject(IProject project);
		Task<bool> RemoveProject(int projectId);
	}
}

