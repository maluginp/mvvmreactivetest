﻿using System;
using ReactiveUI;

namespace MvvmReactiveTest
{
	public interface IButtonsViewModel {
		ReactiveCommand<object> UpdateCommand { get; }
		ReactiveCommand<object> ClearCommand { get; }

		string Label { get; set; }
	}
}

