﻿using System;
using ReactiveUI;
using MvvmReactiveTest.Core.Models;
using System.Collections.Generic;

namespace MvvmReactiveTest.Core.ViewModels {

	public interface IProjectsViewModel {
		ReactiveList<IProjectViewModel> Projects {get;}
		ReactiveCommand<IObservable<IProjectViewModel>> RefreshCommand { get; }
		ReactiveCommand<IEnumerable<IProject>> LoadCommand {get; }
	}
}

