﻿using System;
using System.Collections.Generic;
using MvvmReactiveTest.Core.Models;
using System.Threading.Tasks;

namespace MvvmReactiveTest
{
	public class ProjectsModel
	{
		private IDataService _dataService;
		private IWebService _webService;

		public ProjectsModel () {
			_dataService = new StaticDataService ();
		}

		public Task<IEnumerable<IProject>> List() {
			return _dataService.GetProjects (null);
		}

		public Task<IProject> Get(int projectId) {
			return _dataService.GetProject (projectId);
		}

		public Task<bool> Remove(int projectId) {
			return _dataService.RemoveProject (projectId);
		}

		public async Task<int> SyncronizeRemote() {
//			int syncronized = await _webService.GetProjects ();
			int syncronized = 0;
			return syncronized;
		}
	}
}

