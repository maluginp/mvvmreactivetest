﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using MvvmReactiveTest.Core.Models;
using System.Linq.Expressions;
using MvvmReactiveTest.Models;


namespace MvvmReactiveTest
{
	public class StaticDataService : IDataService
	{
		List<IProject> dataSet = new List<IProject>();

		public StaticDataService () {
			dataSet.Add (new Project () {
				Id = 1,
				Name = "1 Project name",
				Author = "Platon Malyugin",
				Done = false,
				UpdatedAt = DateTime.Now
			});

			dataSet.Add (new Project () {
				Id = 2,
				Name = "2 Project name",
				Author = "2 Platon Malyugin",
				Done = false,
				UpdatedAt = DateTime.Now
			});

			dataSet.Add (new Project () {
				Id = 3,
				Name = "3 Project name",
				Author = "3 Platon Malyugin",
				Done = false,
				UpdatedAt = DateTime.Now
			});

			dataSet.Add (new Project () {
				Id = 4,
				Name = "4 Project name",
				Author = "4 Platon Malyugin",
				Done = false,
				UpdatedAt = DateTime.Now
			});

			dataSet.Add (new Project () {
				Id = 5,
				Name = "5 Project name",
				Author = "5 Platon Malyugin",
				Done = false,
				UpdatedAt = DateTime.Now
			});

		}
		


		public async Task<IEnumerable<IProject>> GetProjects (Expression<Func<IProject, bool>> filter) {
			return dataSet;
		}

		public async Task<IProject> GetProject (int projectId)
		{
			foreach (IProject p in dataSet) {
				if (p.Id == projectId) {
					return p;
				}
			}
			return null;
		}

		public async Task<bool> UpdateProject (IProject project)
		{
			foreach (IProject p in dataSet) {
				if (p.Id == project.Id) {
//					p = project;
					return true;
				}
			}
			return false;
		}

		public async Task<bool> RemoveProject (int projectId)
		{
			IProject project = await GetProject (projectId);
			if (project != null) {
				return dataSet.Remove (project);
			}
			return false;
		}

	}
}

