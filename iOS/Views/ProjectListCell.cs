﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MvvmReactiveTest.Core.Models;
using ReactiveUI;
using System.Reactive.Linq;

namespace MvvmReactiveTest.iOS
{
	public class ProjectListCell : ReactiveTableViewCell, IViewFor<IProjectViewModel>
	{
		public static readonly NSString Key = new NSString ("ProjectListCell");
//		public IProject Model { get; set; } 



		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			this.WhenAny (x => x.ViewModel, vm => vm.Value)
				.Where (x => x != null)
				.Subscribe (x => {
					Console.WriteLine("Write cell: {0}", x.Name);
					TextLabel.Text = x.Name;
					DetailTextLabel.Text = x.Author;
				});
		}
			
		[Export("initWithStyle:reuseIdentifier:")]
		public ProjectListCell(UITableViewCellStyle style, string cellId) 
			: base(UITableViewCellStyle.Subtitle, cellId) {
		}

		IProjectViewModel viewModel;
			
		public IProjectViewModel ViewModel {
			get {
				return viewModel;
			}
			set {
				this.RaiseAndSetIfChanged (ref viewModel, value);
			}
		}

		object IViewFor.ViewModel {
			get { return viewModel; }
			set { viewModel = (IProjectViewModel)value; }
		}
	}
}

