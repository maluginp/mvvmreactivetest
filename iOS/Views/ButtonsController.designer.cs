// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace MvvmReactiveTest.iOS
{
	[Register ("ButtonsController")]
	partial class ButtonsController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnClear { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnUpdate { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel lblText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnUpdate != null) {
				btnUpdate.Dispose ();
				btnUpdate = null;
			}

			if (btnClear != null) {
				btnClear.Dispose ();
				btnClear = null;
			}

			if (lblText != null) {
				lblText.Dispose ();
				lblText = null;
			}
		}
	}
}
