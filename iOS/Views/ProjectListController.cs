﻿
using System;
using System.Drawing;
using System.Reactive;
using System.Reactive.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ReactiveUI;
using MvvmReactiveTest.Core.ViewModels;
using MvvmReactiveTest.ViewModels;

namespace MvvmReactiveTest.iOS
{
	public class ProjectListController : ReactiveTableViewController, IViewFor<IProjectsViewModel>
	{
		UIActivityIndicatorView activityView;
		public ProjectListController () : base (UITableViewStyle.Grouped) {
			
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override async void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Register the TableView's data source
//			var source = new ProjectListSource (TableView);
			ViewModel = new ProjectsViewModel (new ProjectsModel ());

			TableView.RegisterClassForCellReuse (typeof(ProjectListCell), ProjectListCell.Key);
			TableView.Source = new ReactiveTableViewSource<IProjectViewModel> (TableView, ViewModel.Projects, ProjectListCell.Key, 70f, c => {
				Console.WriteLine("Init cell: {0}", c.TextLabel.Text);
			});

			InitActivityIndicator ();

			await ViewModel.LoadCommand.ExecuteAsync ();

		}

		void InitActivityIndicator() {
			activityView = new UIActivityIndicatorView ();
			activityView.Frame = new RectangleF (0, 0, 50, 50);
			activityView.Center = TableView.Center;
			activityView.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			TableView.AddSubview (activityView);

			ViewModel.LoadCommand.IsExecuting.Subscribe (b => {
				if (b) {
					activityView.StartAnimating();
				} else {
					activityView.StopAnimating();
				}
			});

		}

		#region IViewFor
		public IProjectsViewModel _viewModel { get; set;}

		public IProjectsViewModel ViewModel {
			get { return _viewModel; }	
			set { _viewModel = value; }
		}
			
		object IViewFor.ViewModel {
			get { return (object)_viewModel; }
			set { _viewModel = (IProjectsViewModel)value; }
		}
		#endregion

	}
}

