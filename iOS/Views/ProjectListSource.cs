﻿using System;
using System.Drawing;
using ReactiveUI;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MvvmReactiveTest.Core.Models;

namespace MvvmReactiveTest.iOS
{
	public class ProjectListSource : ReactiveTableViewSource<IProject>
	{

		public ProjectListSource (UITableView tableView) : base (tableView)
		{
		}
		


		public override int NumberOfSections (UITableView tableView)
		{
			// TODO: return the actual number of sections
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			// TODO: return the actual number of items in the section
			return 1;
		}

		public override string TitleForHeader (UITableView tableView, int section)
		{
			return "Header";
		}

		public override string TitleForFooter (UITableView tableView, int section)
		{
			return "Footer";
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (ProjectListCell.Key) as ProjectListCell;
//			if (cell == null)
//				cell = new ProjectListCell ();
//
//
//			
			return cell;
		}
	}
}

