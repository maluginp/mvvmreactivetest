﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ReactiveUI;


namespace MvvmReactiveTest.iOS
{

	public partial class ButtonsController : ReactiveViewController, IViewFor<IButtonsViewModel>
	{

		public ButtonsController () : base ("ButtonsController", null) {


		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.OneWayBind(ViewModel, x => x.Label, x => x.lblText.Text); 
			//			this.CommandBind
			this.BindCommand (ViewModel, x => x.UpdateCommand, view => view.btnUpdate);
			this.BindCommand (ViewModel, x => x.ClearCommand, view => view.btnClear);

			ViewModel = new ButtonsViewModel ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

//		private IButtonsViewModel _dataContext;


		IButtonsViewModel _ViewModel;
		public IButtonsViewModel ViewModel {
			get { return _ViewModel; }
			set { this.RaiseAndSetIfChanged(ref _ViewModel, value); }
		}

		object IViewFor.ViewModel {
			get { return ViewModel; }
			set { ViewModel = (IButtonsViewModel)value; }
		}


	}
}

