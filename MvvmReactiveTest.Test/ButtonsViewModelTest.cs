﻿using System;
using NUnit.Framework;
using MvvmReactiveTest.ViewModels;
//using ReactiveUI;

namespace MvvmReactiveTest.Test
{
	[TestFixture]
	public class ButtonsViewModelTest
	{
		ButtonsViewModel _vm =  new ButtonsViewModel();

		[Test]
		public void UpdateCommandTest () {
			Assert.IsTrue (_vm.UpdateCommand.CanExecute (null));
			_vm.UpdateCommand.Execute (null);

			Assert.AreEqual (_vm.Label, "Updated");
		}

		[Test]
		public void ClearCommandTest() {
			Assert.IsTrue (_vm.ClearCommand.CanExecute (null));
			_vm.ClearCommand.Execute (null);
			Assert.AreEqual (_vm.Label, string.Empty);
		}


	}
}
